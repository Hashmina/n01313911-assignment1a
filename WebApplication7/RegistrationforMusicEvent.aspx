﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrols.aspx.cs" Inherits="webformintro.servercontrols" %>
<!DOCTYPE html>

<head runat="server">
<title>Registration for Music Event</title>
</head>
<body>
    <form id="form2" runat="server">
        <div>
            <p>Register for Music Event</p>
            <asp:TextBox runat="server" ID="Name" placeholder="Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Name:" ControlToValidate="Name" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="Email" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your Email address" ControlToValidate="Email" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="Email" ErrorMessage="You've entered Invalid Email"></asp:RegularExpressionValidator>
            <br/>
            <br />
            <asp:TextBox runat="server" ID="Phone" placeholder="Phone"></asp:TextBox>
            <br/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Phone Number:" ControlToValidate="Phone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="You've entred invalid phone number"></asp:CompareValidator>
            <br />
            <br />
            <asp:TextBox runat="server" ID="Address" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="Address" ErrorMessage="Enter your address"></asp:RequiredFieldValidator>
            <br />
            <asp:DropDownList runat="server" ID="ageselect">
                <asp:ListItem Value="18-25" Text="18-25"></asp:ListItem>
                <asp:ListItem Value="26-35" Text="26-35"></asp:ListItem>
                <asp:ListItem Value="36-55" Text="36-55"></asp:ListItem>
            </asp:DropDownList>
            <br />
            
            <asp:RadioButton id= "radiooopt1" runat="server" Text="Number of people" GroupName="via"/>
            
            <br />
            <div id="people" runat="server">
            <asp:CheckBox runat="server" ID="p1" Text="1 Adult" />
            <asp:CheckBox runat="server" ID="p2" Text="2 Adults" />
            <asp:CheckBox runat="server" ID="p3" Text="3 Adults " />
            </div>

            <br />
            <asp:DropDownList runat="server" ID="DropDownList1">
                <asp:ListItem Value="r1" Text="Row 1"></asp:ListItem>
                <asp:ListItem Value="r2" Text="Row 2"></asp:ListItem>
                <asp:ListItem Value="r3" Text="Row 3"></asp:ListItem>
                <asp:ListItem Value="r4" Text="Row 4"></asp:ListItem>
                <asp:ListItem Value="r5" Text="Row 5"></asp:ListItem>
                <asp:ListItem Value="r6" Text="Row 6"></asp:ListItem>
                <asp:ListItem Value="r7" Text="Row 7"></asp:ListItem>
                <asp:ListItem Value="r8" Text="Row 8"></asp:ListItem>
                <asp:ListItem Value="r9" Text="Row 9"></asp:ListItem>
                <asp:ListItem Value="r10" Text="Row 10"></asp:ListItem>
            </asp:DropDownList>
            
            <br />

            <asp:DropDownList runat="server" ID="DropDownList2">
                <asp:ListItem Value="r1" Text="Credit Card"></asp:ListItem>
                <asp:ListItem Value="r2" Text="Debit Card"></asp:ListItem>
                <asp:ListItem Value="r3" Text="Net Banking"></asp:ListItem>
                
            </asp:DropDownList>
            <br />
            <asp:Button runat="server" ID="myButton" OnClick="Register" Text="Submit"/>
            <br />
            <div runat="server" ID="res"></div>
            
        </div>
    </form>
</body>
</html>